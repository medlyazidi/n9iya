<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisponibilitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disponibilites', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_disponibilite');

            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id_client')->on('clients');

            $table->integer('id_employee')->unsigned();
            $table->foreign('id_employee')->references('id_employee')->on('employees');

            $table->integer('jour');
            $table->dateTime('date_debut');
            $table->dateTime('date_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disponibilites');
    }
}
