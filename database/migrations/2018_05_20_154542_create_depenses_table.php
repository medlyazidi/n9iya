<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depenses', function (Blueprint $table) {
            $table->increments('id_depense');

            $table->integer('id_employee')->unsigned();
            $table->foreign('id_employee')->references('id_employee')->on('employees');

            $table->integer('id_mode_paiement')->unsigned();
            $table->foreign('id_mode_paiement')->references('id_mode_paiement')->on('mode_paiements');

            $table->integer('id_type_depense')->unsigned();
            $table->foreign('id_type_depense')->references('id_type_depense')->on('type_depense');

            $table->float('solde');
            $table->dateTime('date_reception');
            $table->dateTime('date_encaissement');
            $table->string('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depenses');
    }
}
