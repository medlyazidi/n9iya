<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRessourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ressources', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_ressource');

            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id_client')->on('clients');

            $table->integer('id_mode_paiement')->unsigned();
            $table->foreign('id_mode_paiement')->references('id_mode_paiement')->on('mode_paiements');

            $table->integer('id_abonnement')->unsigned();
            $table->foreign('id_abonnement')->references('id_abonnement')->on('abonnements');

            $table->dateTime('date_reception');
            $table->dateTime('date_encaissement');
            $table->string('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ressources');
    }
}
