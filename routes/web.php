<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

//Auth::routes();
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');


//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','HomeController@home')->name('home_user');
Route::post('/','MessageController@commande')->name('commande');

Route::get('/index', 'IndexController@index')->name('index');


Route::get('/index2', 'IndexController@index2');

Route::group(['prefix'=>'admin'],function(){
        /*|-------------------------------------------------|
          |                 GESTION DES CLIENTS             |
          |-------------------------------------------------|*/

    Route::group(['prefix'=>'clients'],function(){
        Route::get('/','ClientController@liste')->name('list_client');
        Route::get('create','ClientController@create')->name('create_client');
        Route::post('add','ClientController@add');
        Route::get('edit/{id_client}','ClientController@edit')->name('edit_client');
        Route::post('update','ClientController@update')->name('update_client');
        Route::get('delete', 'ClientController@destroy')->name('delete_client');
    });

    Route::group(['prefix'=>'employees'],function(){
        Route::get('/','EmployeeController@liste')->name('list_employee');
        Route::get('create','EmployeeController@create')->name('create_employee');
        Route::post('add','EmployeeController@add');
        Route::get('edit/{id_employee}','EmployeeController@edit')->name('edit_employee');
        Route::post('update','EmployeeController@update')->name('update_employee');
        Route::get('delete', 'EmployeeController@destroy')->name('delete_employee');
    });

    Route::group(['prefix'=>'mode_paiements'],function(){
        Route::get('/','ModePaiementController@liste')->name('list_mode_paiement');
        Route::get('create','ModePaiementController@create')->name('create_mode_paiement');
        Route::post('add','ModePaiementController@add');
        Route::get('edit/{id_mode_paiement}','ModePaiementController@edit')->name('edit_mode_paiement');
        Route::post('update','ModePaiementController@update')->name('update_mode_paiement');
        Route::get('delete', 'ModePaiementController@destroy')->name('delete_mode_paiement');
    });

    Route::group(['prefix'=>'abonnements'],function(){
        Route::get('/','AbonnementController@liste')->name('list_abonnement');
        Route::get('create','AbonnementController@create')->name('create_abonnement');
        Route::post('add','AbonnementController@add');
        Route::get('edit/{id_abonnement}','AbonnementController@edit')->name('edit_abonnement');
        Route::post('update','AbonnementController@update')->name('update_abonnement');
        Route::get('delete','AbonnementController@destroy')->name('delete_abonnement');
    });

    Route::group(['prefix'=>'type_depenses'],function(){
        Route::get('/','TypeDepenseController@liste')->name('list_type_depense');
        Route::get('create','TypeDepenseController@create')->name('create_type_depense');
        Route::post('add','TypeDepenseController@add');
        Route::get('edit/{id_type_depense}','TypeDepenseController@edit')->name('edit_type_depense');
        Route::post('update','TypeDepenseController@update')->name('update_type_depense');
        Route::get('delete','TypeDepenseController@destroy')->name('delete_type_depense');
    });

    Route::group(['prefix'=>'depenses'],function(){
        Route::get('/','DepenseController@liste')->name('list_depense');
        Route::get('create','DepenseController@create')->name('create_depense');
        Route::post('add','DepenseController@add');
        Route::get('edit/{id_depense}','DepenseController@edit')->name('edit_depense');
        Route::post('update','DepenseController@update')->name('update_depense');
        Route::get('delete','DepenseController@destroy')->name('delete_depense');
    });
	Route::group(['prefix'=>'ressources'],function(){
        Route::get('/','RessourceController@liste')->name('list_ressource');
        Route::get('create','RessourceController@create')->name('create_ressource');
        Route::post('add','RessourceController@add');
        Route::get('edit/{id_ressource}','RessourceController@edit')->name('edit_ressource');
        Route::post('update','RessourceController@update')->name('update_ressource');
        Route::get('delete','RessourceController@destroy')->name('delete_ressource');
    });
});

/*
Route::group(['prefix'=>'user'],function(){
    Route::get('home/','HomeController@home')->name('home_user');
    Route::post('home/','MessageController@commande')->name('commande');

});
*/