<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ressource extends Model
{
    protected $fillable = [
        'id_ressource', 'id_client', 'id_mode_paiement', 'id_abonnement', 'date_reception', 'date_encaissement', 'desc', 'created_at', 'updated_at'
    ];
}
