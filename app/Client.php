<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'id_client', 'nom', 'prenom', 'sexe', 'email', 'telephone', 'adresse', 'cin', 'created_at', 'updated_at'
    ];
}
