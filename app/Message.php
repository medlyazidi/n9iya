<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'id_message', 'nom', 'tel', 'mail', 'vu', 'id_abonnement', 'message', 'created_at', 'updated_at'
    ];
}
