<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModePaiement extends Model
{
    protected $fillable = [
        'id_mode_paiement', 'libele_mp', 'created_at', 'updated_at'
    ];
}
