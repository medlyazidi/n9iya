<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    protected $fillable = [
        'id_abonnement', 'titre', 'duree', 'descreptif', 'solde', 'created_at', 'updated_at'
    ];
}
