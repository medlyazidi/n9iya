<?php

namespace App\Http\Controllers;

use App\Abonnement;
use Illuminate\Http\Request;

class AbonnementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('page.abonnements.ajouter_abonnement');
    }
    public function add(Request $request){
        //dd($request->all());
        Abonnement::create($request->except(['_token']));
        return redirect(route('list_abonnement'));
    }
    public function liste(){
        $abonnements = Abonnement::get();
        return view('page.abonnements.list_abonnement',compact('abonnements'));
    }
    public function edit($id_abonnement){
        $abonnement = Abonnement::where('id_abonnement', $id_abonnement)->first();
        return view('page.abonnements.edit_abonnement',compact('abonnement'));
    }
    public function update(Request $request){
        Abonnement::where('id_abonnement',$request->id_abonnement)->update($request->except(['_token']));
        return redirect(route('list_abonnement'));
    }
    public function destroy(Request $request){
        Abonnement::where('id_abonnement',$request->id_abonnement)->delete();
        return redirect(route('list_abonnement'));
    }
}
