<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('page.employees.ajouter_employee');
    }
    public function add(Request $request){
        Employee::create($request->all());
        return redirect(route('list_employee'));
    }
    public function liste(){
        $employees = Employee::get();
        return view('page.employees.list_employee',compact('employees'));
    }
    public function edit($id_employee){
        $employee = Employee::where('id_employee', $id_employee)->first()  ;
        return view('page.employees.edit_employee',compact('employee'));
    }
    public function update(Request $request){
        Employee::where('id_employee',$request->id_employee)->update($request->except(['_token']));
        return redirect(route('list_employee'));
    }
    public function destroy(Request $request){
        Employee::where('id_employee',$request->id_employee)->delete();
        return redirect(route('list_employee'));
    }
}
