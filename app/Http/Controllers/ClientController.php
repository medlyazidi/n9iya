<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('page.clients.ajouter_client');
    }
    public function add(Request $request){
        Client::create($request->all());
        return redirect(route('list_client'));
    }
    public function liste(){
        $clients = Client::get();
     return view('page.clients.list_client',compact('clients'));
    }
    public function edit($id_client){
        $client = Client::where('id_client', $id_client)->first()  ;
        return view('page.clients.edit_client',compact('client'));
    }
    public function update(Request $request){
        Client::where('id_client',$request->id_client)->update($request->except(['_token']));
        return redirect(route('list_client'));
    }
    public function destroy(Request $request){
        Client::where('id_client',$request->id_client)->delete();
        return redirect(route('list_client'));
    }

}
