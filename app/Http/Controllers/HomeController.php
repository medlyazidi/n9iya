<?php

namespace App\Http\Controllers;

use App\Abonnement;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	
	public function home(){
       // dd('test');
        $abonnements = Abonnement::get();
        //dd($abonnements);
		return view('page.user.home', compact('abonnements'));
	}
}
