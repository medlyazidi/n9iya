<?php

namespace App\Http\Controllers;

use App\TypeDepense;
use Illuminate\Http\Request;

class TypeDepenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('page.type_depenses.ajouter_type_depense');
    }
    public function add(Request $request){
        //dd($request->all());
        TypeDepense::create($request->except(['_token']));
        return redirect(route('list_type_depense'));
    }
    public function liste(){
        $type_depenses = TypeDepense::get();
        return view('page.type_depenses.list_type_depense',compact('type_depenses'));
    }
    public function edit($id_type_depense){
        $type_depense = TypeDepense::where('id_type_depense', $id_type_depense)->first();
        return view('page.type_depenses.edit_type_depense',compact('type_depense'));
    }
    public function update(Request $request){
        TypeDepense::where('id_type_depense',$request->id_type_depense)->update($request->except(['_token']));
        return redirect(route('list_type_depense'));
    }
    public function destroy(Request $request){
        TypeDepense::where('id_type_depense',$request->id_type_depense)->delete();
        return redirect(route('list_type_depense'));
    }
}
