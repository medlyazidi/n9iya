<?php

namespace App\Http\Controllers;

use App\ModePaiement;
use Illuminate\Http\Request;

class ModePaiementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('page.mode_paiements.ajouter_mode_paiement');
    }
    public function add(Request $request){
        //dd($request->all());
        ModePaiement::create($request->except(['_token']));
        return redirect(route('list_mode_paiement'));
    }
    public function liste(){
        $mode_paiements = ModePaiement::get();
        return view('page.mode_paiements.list_mode_paiement',compact('mode_paiements'));
    }
    public function edit($id_mode_paiement){
        $mode_paiement = ModePaiement::where('id_mode_paiement', $id_mode_paiement)->first();
        return view('page.mode_paiements.edit_mode_paiement',compact('mode_paiement'));
    }
    public function update(Request $request){
        ModePaiement::where('id_mode_paiement',$request->id_mode_paiement)->update($request->except(['_token']));
        return redirect(route('list_mode_paiement'));
    }
    public function destroy(Request $request){
        ModePaiement::where('id_mode_paiement',$request->id_mode_paiement)->delete();
        return redirect(route('list_mode_paiement'));
    }
}
