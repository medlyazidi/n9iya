<?php

namespace App\Http\Controllers;

use App\Depense;
use App\Employee;
use App\ModePaiement;
use App\TypeDepense;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class DepenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
		$employees = Employee::get();
		$mode_paiements = ModePaiement::get();
		$type_depenses = TypeDepense::get();
        return view('page.depenses.ajouter_depense', compact('employees','mode_paiements','type_depenses'));
    }
    public function add(Request $request){

        Depense::create($request->all());
        return redirect(route('list_depense'));
    }
    public function liste(){
		
		$depenses = DB::table('depenses')
                ->join('mode_paiements', 'depenses.id_mode_paiement', '=', 'mode_paiements.id_mode_paiement')
                ->join('type_depenses', 'depenses.id_type_depense', '=', 'type_depenses.id_type_depense')
                ->join('employees', 'depenses.id_employee', '=', 'employees.id_employee')
                ->select('depenses.*','mode_paiements.libele_mp','type_depenses.libele_td','employees.*')
                ->groupBy('depenses.id_depense')
                ->get();
        return view('page.depenses.list_depense',compact('depenses'));
    }
    public function edit($id_depense){
        $depense = Depense::where('id_depense', $id_depense)->first()  ;
        return view('page.depenses.edit_depense',compact('depense'));
    }
    public function update(Request $request){
        Depense::where('id_depense',$request->id_depense)->update($request->except(['_token']));
        return redirect(route('list_depense'));
    }
    public function destroy(Request $request){
        Depense::where('id_depense',$request->id_depense)->delete();
        return redirect(route('list_depense'));
    }

}
