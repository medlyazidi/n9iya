<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depense extends Model
{
    protected $fillable = [
        'id_depense', 'id_employee', 'id_mode_paiement', 'id_type_depense', 'solde', 'date_reception', 'date_encaissement', 'desc', 'created_at', 'updated_at'
    ];
}
