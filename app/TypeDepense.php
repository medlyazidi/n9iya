<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeDepense extends Model
{
    protected $fillable = [
        'id_type_depense', 'libele_td', 'created_at', 'updated_at'
    ];
}
