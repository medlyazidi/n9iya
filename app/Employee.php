<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'id_employee', 'nom', 'prenom', 'sexe', 'email', 'telephone', 'adresse', 'cin', 'created_at', 'updated_at'
    ];
}
