<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Article</title>

    <link href="{{URL::asset('public/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('public/admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{URL::asset('public/admin/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">


    <link href="{{URL::asset('public/admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('public/admin/css/style.css')}}" rel="stylesheet">

    @yield('header_script')

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{URL::asset('public/admin/img/profile_small.jpg')}}" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href="{{route('list_client')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Client</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_client')}}">Listes des Clients</a></li>
                        <li><a href="{{route('create_client')}}">Ajouter un client</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('list_employee')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Employee</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_employee')}}">Listes des Employees</a></li>
                        <li><a href="{{route('create_employee')}}">Ajouter un employee</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('list_mode_paiement')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Mode de Paiement</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_mode_paiement')}}">Listes des modes paiement</a></li>
                        <li><a href="{{route('create_mode_paiement')}}">Ajouter un mode de paiement</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('list_abonnement')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Abonnements</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_abonnement')}}">Listes des abonnements</a></li>
                        <li><a href="{{route('create_abonnement')}}">Ajouter abonnements</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('list_type_depense')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Type Depense</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_type_depense')}}">Listes des types depenses</a></li>
                        <li><a href="{{route('create_type_depense')}}">Ajouter un type depense</a></li>
                    </ul>
                </li>
				<li>
                    <a href="{{route('list_depense')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Depense</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_depense')}}">Listes des depenses</a></li>
                        <li><a href="{{route('create_depense')}}">Ajouter une depense</a></li>
                    </ul>
                </li>
                <li>
                    <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">Layouts</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graphs</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="graph_flot.html">Flot Charts</a></li>
                        <li><a href="graph_morris.html">Morris.js Charts</a></li>
                        <li><a href="graph_rickshaw.html">Rickshaw Charts</a></li>
                        <li><a href="graph_chartjs.html">Chart.js</a></li>
                        <li><a href="graph_chartist.html">Chartist</a></li>
                        <li><a href="c3.html">c3 charts</a></li>
                        <li><a href="graph_peity.html">Peity Charts</a></li>
                        <li><a href="graph_sparkline.html">Sparkline Charts</a></li>
                    </ul>
                </li>




            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="login.html">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            @yield('head')

        </div>


        @yield('content')


        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2015
            </div>
        </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{URL::asset('public/admin/js/jquery-2.1.1.js')}}"></script>
<script src="{{URL::asset('public/admin/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{URL::asset('public/admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{URL::asset('public/admin/js/inspinia.js')}}"></script>
<script src="{{URL::asset('public/admin/js/plugins/pace/pace.min.js')}}"></script>

@yield('footer_script')

</body>

</html>
