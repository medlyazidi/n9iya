@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>Sponsors</h2>
        <ol class="breadcrumb">
            <li>
                <a href="../admin/dashboard">admin</a>
            </li>
            <li>
                <a>Sponsor</a>
            </li>
            <li class="active">
                <strong>Liste</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ajouter ville<small></small></h5>

            </div>
            <div class="ibox-content">
                <form method="post" action="secteur/save" class="form-horizontal" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Région</label>

                        <div class="col-sm-10">
                            <input type="text" name="region" class="form-control" value ="sjjhg" disabled="disabled"/>
                            <input type="hidden" name="id" class="form-control" value ="sdj"/>
                        </div><br/>


                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Nom du Secteur</label>

                        <div class="col-sm-10"><input type="text" name="nom"  class="form-control" required="required"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection