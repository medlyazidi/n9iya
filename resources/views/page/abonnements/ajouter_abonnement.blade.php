@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>Abonnement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Ajoute</a>
            </li>
            <li class="active">
                <strong>Abonnement</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ajouter Abonnement
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="add" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
							<div class="form-group"><label class="col-sm-2 control-label">Titre
                                    :</label>
                                <div class="col-sm-10"><input type="text" name="titre" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">La duree de l'abonnement
                                    :</label>
                                <div class="col-sm-10"><input type="float" name="duree" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Le solde :</label>
                                <div class="col-sm-10"><input type="float" name="solde" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">La discription de l'abonnement
                                    :</label>
                                <div class="col-sm-10"><textarea name="descreptif" class="form-control"
                                                                 required="required"></textarea>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Ajouter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection