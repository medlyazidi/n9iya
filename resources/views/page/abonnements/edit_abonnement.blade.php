@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>Abonnement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Editer</a>
            </li>
            <li class="active">
                <strong>Abonnement</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Editer Abonnement
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{route('update_abonnement')}}" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="id_abonnement" value="{{$abonnement->id_abonnement}}"/>
							<div class="form-group"><label class="col-sm-2 control-label">Titre
                                    :</label>
                                <div class="col-sm-10"><input type="text" name="titre" class="form-control"
                                                              value="{{$abonnement->titre}}" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">La duree de l'abonnement
                                    :</label>
                                <div class="col-sm-10"><input type="float" name="duree" class="form-control"
                                                              value="{{$abonnement->duree}}" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Le solde :</label>
                                <div class="col-sm-10"><input type="float" name="solde" class="form-control"
                                                              value="{{$abonnement->solde}}" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">La discription de l'abonnement
                                    :</label>
                                <div class="col-sm-10"><textarea name="descreptif" class="form-control"
                                                                 required="required">{{$abonnement->descreptif}}</textarea>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Editer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection