@extends('admin')

@section('header_script')
    <!-- Sweet Alert -->
    <link href="{{URL::asset('public/admin/css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection

@section('head')
    <div class="col-lg-10">
        <h2>Abonnement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Liste</a>
            </li>
            <li class="active">
                <strong>Abonnements</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <br><br>
        <button class="btn btn pull-right"><a href="{{route('create_abonnement')}}"><strong>Ajoute
                    abonnement</strong></a></button>
    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="row">
                @foreach($abonnements as $abonnement)
                    <div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-content product-box">

                                <div class="product-imitation">
                                    [ INFO ]
                                </div>
                                <div class="product-desc">
                                <span class="product-price">
                                    {{$abonnement->solde}} DH
                                </span>
                                    <small class="text-muted">{{$abonnement->titre}}</small>
                                    <a href="#" class="product-name"> {{$abonnement->duree}}</a>


                                    <div class="small m-t-xs">
                                        {{$abonnement->descreptif}}
                                    </div>
                                    <div class="m-t text-righ">

                                        <button class="btn btn-xs btn-outline btn-danger delete-demmo" value="delete_abonnement{!! $abonnement->id_abonnement !!}">Supp <i
                                                    class="fa fa-long-arrow-right"></i> </button>
													
											<form id="delete_abonnement{!! $abonnement->id_abonnement !!}"
                                                          action="{!! route('delete_abonnement') !!}" style="display: none">
                                                        {{ csrf_field() }}
                                                <input name="id_abonnement" type="hidden" class="form-control"
                                                               value="{!! $abonnement->id_abonnement !!}">
                                                <button type="submit" class="btn btn-default demoTest" id="btn-submit">
                                                            Delete
                                                </button>
                                            </form>
                                        <a href="{{route('edit_abonnement', $abonnement->id_abonnement)}}" class="btn btn-xs btn-outline btn-warning">Edit<i
                                                    class="fa fa-long-arrow-right"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <!-- Sweet alert -->
    <script src="{{URL::asset('public/admin/js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            $('.delete-demmo').click(function () {
				
                var idForm = "#" + $(this).attr("value");
				
				console.log(idForm);

                swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to delete this photo?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "Not,don't delete it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your file was successfully deleted!", "success");
                            $(idForm).submit();
                        } else {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        }
                    });


            });
        });
    </script>
@endsection