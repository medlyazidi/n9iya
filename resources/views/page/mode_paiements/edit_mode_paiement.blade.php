@extends('admin')


@section('head')
    <div class="col-lg-10">
        <h2>Mode de paiement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Modifier</a>
            </li>
            <li class="active">
                <strong>mode de paiement</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Modifier le mode de paiement
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{route('update_mode_paiement')}}" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_mode_paiement" value="{{$mode_paiement->id_mode_paiement}}"/>
                            <div class="form-group"><label class="col-sm-2 control-label">Libelé mode_paiement :</label>
                                <div class="col-sm-10"><input type="text" name="libele_mp" class="form-control"
                                                              value="{{$mode_paiement->libele_mp}}" required="required">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Modfier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

