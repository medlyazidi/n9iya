@extends('admin')

@section('header_script')
    <!-- Sweet Alert -->
    <link href="{{URL::asset('public/admin/css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection

@section('head')
    <div class="col-lg-10">
        <h2>Mode de Paiement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Liste</a>
            </li>
            <li class="active">
                <strong>modes paiement</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    <!--
        <br><br>
        <button class="btn btn pull-right"  ><a href="{{route('create_client')}}"><strong>Ajoute Client</strong></a></button>
        -->
    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            @foreach($mode_paiements as $mode_paiement)
                <div class="col-lg-4">
                    <div class="contact-box">
                    <!--<a href="{{route('edit_mode_paiement', $mode_paiement->id_mode_paiement)}}">-->
                        <div class="col-sm-4">
                            <div class="text-center">
                                <img alt="image" class="img-circle m-t-xs img-responsive" src="img/a3.jpg">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h2><strong> {{$mode_paiement->libele_mp}}</strong></h2>
                        </div>
                        <div class="col-sm-2" style="margin-top: 6%">
                            <img class="img-circle m-t-xs img-responsive delete-demmo"
                                 value="delete_mode_paiement{!! $mode_paiement->id_mode_paiement !!}"
                                 alt="delete_mode_paiement{!!$mode_paiement->id_mode_paiement !!}" src="img/x.png">
                        </div>
                        <div class="clearfix">
                            <form id="delete_mode_paiement{!! $mode_paiement->id_mode_paiement !!}"
                                  action="{!! route('delete_mode_paiement') !!}" style="display: none">
                                {{ csrf_field() }}
                                <input name="id_mode_paiement" type="hidden" class="form-control"
                                       value="{!! $mode_paiement->id_mode_paiement !!}">
                                <button class="btn btn-default demoTest" id="btn-submit">Delete</button>
                            </form>
                        </div>
                        <!--</a>-->
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('footer_script')
    <!-- Sweet alert -->
    <script src="{{URL::asset('public/admin/js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            $('.delete-demmo').click(function () {

                var idForm = "#" + $(this).attr("alt");

                swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to delete this photo?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "Not,don't delete it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your file was successfully deleted!", "success");
                            $(idForm).submit();
                        } else {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        }
                    });


            });
        });
    </script>
@endsection