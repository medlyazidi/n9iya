@extends('admin')

@section('header_script')
    <!-- Sweet Alert -->
    <link href="{{URL::asset('public/admin/css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection

@section('head')
    <div class="col-lg-10">
        <h2>Client</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Liste</a>
            </li>
            <li class="active">
                <strong>clients</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <br><br>
        <button class="btn btn pull-right"><a href="{{route('create_client')}}"><strong>Ajoute Client</strong></a>
        </button>
    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="ibox">
                <div class="ibox-content">
                    <span class="text-muted small pull-right">Last modification: <i class="fa fa-clock-o"></i> 2:10 pm - 12.06.2014</span>
                    <h2>Clients</h2>
                    <p>
                        All clients need to be verified before you can send email and set a project.
                    </p>
                    <div class="input-group">
                        <input type="text" placeholder="Search client " class="input form-control">
                        <span class="input-group-btn">
                                        <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                                </span>
                    </div>
                    <div class="clients-list">
                        <div class="tab-content">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                        @foreach($clients as $client)
                                            <tr>
                                                <td class="client-avatar"><img class="delete-demmo"
                                                                               value="delete_client{!! $client->id_client !!}"
                                                                               alt="delete_client{!! $client->id_client !!}"
                                                                               src="img/x.png">
                                                    <form id="delete_client{!! $client->id_client !!}"
                                                          action="{!! route('delete_client') !!}" style="display: none">
                                                        {{ csrf_field() }}
                                                        <input name="id_client" type="hidden" class="form-control"
                                                               value="{!! $client->id_client !!}">
                                                        <button class="btn btn-default demoTest" id="btn-submit">
                                                            Delete
                                                        </button>
                                                    </form>
                                                </td>
                                                <td class="client-avatar"><img alt="image" src="img/a3.jpg"></td>
                                                <td><a href="{{route('edit_client', $client->id_client)}}"
                                                       class="client-link">{{$client->nom}} {{$client->prenom}}</a></td>
                                                <td> Sala Jadida</td>
                                                <td><i class="fa fa-phone"> </i> {{$client->telephone}}</td>
                                                <td><i class="fa fa-map-marker"></i> {{$client->adresse}}</td>
                                                <td class="client-status"><span
                                                            class="label label-primary">Active</span></td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- delete client-->

@endsection

@section('footer_script')
    <!-- Sweet alert -->
    <script src="{{URL::asset('public/admin/js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            $('.delete-demmo').click(function () {

                var idForm = "#" + $(this).attr("alt");

                swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to delete this photo?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "Not,don't delete it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your file was successfully deleted!", "success");
                            $(idForm).submit();
                        } else {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        }
                    });


            });
        });
    </script>
@endsection