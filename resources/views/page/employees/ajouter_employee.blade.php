@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>employee</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Ajoute</a>
            </li>
            <li class="active">
                <strong>employee</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ajouter employee
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="add" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group"><label class="col-sm-2 control-label">Nom du employee :</label>
                                <div class="col-sm-10"><input type="text" name="nom" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Prenom du employee :</label>
                                <div class="col-sm-10"><input type="text" name="prenom" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Sexe / Entreprise :</label>

                                <div class="col-sm-10"><select class="form-control m-b" name="sexe">
                                        <option value="madame">Madame</option>
                                        <option value="Monsieur">Monsieur</option>
                                        <option value="entreprise">Entreprise</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">E-Mail :</label>
                                <div class="col-sm-10"><input type="text" name="email" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Téléphone :</label>
                                <div class="col-sm-10"><input type="text" name="telephone" class="form-control"
                                                              required="required"></div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Adresse :</label>
                                <div class="col-sm-10"><textarea name="adresse" class="form-control"
                                                                 required="required"></textarea></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">N° Carte d'Identité Nationnal
                                    :</label>
                                <div class="col-sm-10"><input type="text" name="cin" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Ajouter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection