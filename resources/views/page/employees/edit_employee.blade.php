@extends('admin')


@section('head')
    <div class="col-lg-10">
        <h2>Employee</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Ajoute</a>
            </li>
            <li class="active">
                <strong>employee</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ajouter employee
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{route('update_employee')}}" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_employee" value="{{$employee->id_employee}}"/>
                            <div class="form-group"><label class="col-sm-2 control-label">Nom du employee :</label>
                                <div class="col-sm-10"><input type="text" name="nom" class="form-control"
                                                              value="{{$employee->nom}}" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Prenom du employee :</label>
                                <div class="col-sm-10"><input type="text" name="prenom" value="{{$employee->prenom}}"
                                                              class="form-control" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Sexe / Entreprise :</label>

                                <div class="col-sm-10"><select class="form-control m-b" name="sexe">
                                        <option value="madame" @if($employee->sexe == "madame") selected @endif>Madame
                                        </option>
                                        <option value="Monsieur" @if($employee->sexe == "Monsieur") selected @endif>
                                            Monsieur
                                        </option>
                                        <option value="entreprise" @if($employee->sexe == "entreprise") selected @endif>
                                            Entreprise
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">E-Mail :</label>
                                <div class="col-sm-10"><input type="text" name="email" value="{{$employee->email}}"
                                                              class="form-control" required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Téléphone :</label>
                                <div class="col-sm-10"><input type="text" name="telephone"
                                                              value="{{$employee->telephone}}" class="form-control"
                                                              required="required"></div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Adresse :</label>
                                <div class="col-sm-10"><textarea name="adresse" class="form-control"
                                                                 required="required">{{$employee->adresse}}</textarea>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">N° Carte d'Identité Nationnal
                                    :</label>
                                <div class="col-sm-10"><input type="text" name="cin" value="{{$employee->cin}}"
                                                              class="form-control" required="required"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Modfier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

