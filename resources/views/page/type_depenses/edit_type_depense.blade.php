@extends('admin')


@section('head')
    <div class="col-lg-10">
        <h2>Type Depense</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Modifier</a>
            </li>
            <li class="active">
                <strong>Type Depense</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Modifier le Type Depense
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{route('update_type_depense')}}" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_type_depense" value="{{$type_depense->id_type_depense}}"/>
                            <div class="form-group"><label class="col-sm-2 control-label">Libelé type_depense :</label>
                                <div class="col-sm-10"><input type="text" name="libele_td" class="form-control"
                                                              value="{{$type_depense->libele_td}}" required="required">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Modfier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

