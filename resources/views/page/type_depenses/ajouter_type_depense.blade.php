@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>Type Depense</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Ajoute</a>
            </li>
            <li class="active">
                <strong>Type Depense</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ajouter Type Depense
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="add" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group"><label class="col-sm-2 control-label">Libilé du Type Depense :</label>
                                <div class="col-sm-10"><input type="text" name="libele_td" class="form-control"
                                                              required="required"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Ajouter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection