@extends('admin')

@section('header_script')
    <!-- Sweet Alert -->
    <link href="{{URL::asset('public/admin/css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection

@section('head')
    <div class="col-lg-10">
        <h2>Client</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Liste</a>
            </li>
            <li class="active">
                <strong>clients</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <br><br>
        <button class="btn btn pull-right"><a href="{{route('create_client')}}"><strong>Ajoute Client</strong></a>
        </button>
    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Basic Data Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>#</th>
									<th>Type de depense</th>
									<th>mode de paiement</th>
                                    <th>Solde</th>
                                    <th>Date reception</th>
                                    <th>Date encaissement</th>
									<th>Nom et Prenom</th>
									<th>Telephone</th>
									<th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($depenses as $key => $depense)
                                        <tr class="gradeC">
											<td>{{$key+1}}</td>
											<td>{{$depense->libele_td}}</td>
											<td>{{$depense->libele_mp}}</td>
											<td class="center">{{$depense->solde}}</td>
											<td>{{$depense->date_reception}}</td>
											<td>{{$depense->date_encaissement}}</td>
											<td class="center">{{$depense->nom}} {{$depense->prenom}}</td>
											<td class="center">{{$depense->telephone}}</td>
											<td>
											<button class="btn btn-xs btn-outline btn-danger delete-demmo" value="delete_depense{{$depense->id_depense}}">Supp <i
														class="fa fa-long-arrow-right"></i> </button>
												<form id="delete_depense{{$depense->id_depense}}"
															  action="{!! route('delete_depense') !!}" style="display: none">
															{{ csrf_field() }}
													<input name="id_depense" type="hidden" class="form-control"
																   value="{!! $depense->id_depense !!}">
													<button type="submit" class="btn btn-default demoTest" id="btn-submit">
																Delete
													</button>
												</form>
											<a href="{{route('edit_depense', $depense->id_depense)}}" class="btn btn-xs btn-outline btn-warning">Edit<i
														class="fa fa-long-arrow-right"></i> </a>
											</td>
										</tr>
									@endforeach()
                                
                                
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
									<th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
									<th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- delete client-->

@endsection

@section('footer_script')
    <!-- Sweet alert -->
    <script src="{{URL::asset('public/admin/js/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{URL::asset('public/admin/js/plugins/jeditable/jquery.jeditable.js')}}"></script>
    <script src="{{URL::asset('public/admin/js/plugins/dataTables/datatables.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            $('.delete-demmo').click(function () {

				
                var idForm = "#" + $(this).attr("value");
				
                swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to delete this photo?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "Not,don't delete it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your file was successfully deleted!", "success");
                            $(idForm).submit();
                        } else {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        }
                    });


            });

            $('.dataTables-example').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        });
    </script>
@endsection