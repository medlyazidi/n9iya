@extends('admin')

@section('head')
    <div class="col-lg-10">
        <h2>Depense</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a>Ajoute</a>
            </li>
            <li class="active">
                <strong>Ressource</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
@endsection


@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ajouter Ressource
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="add" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            
                            <div class="form-group"><label class="col-sm-2 control-label">Clients :</label>

                                <div class="col-sm-10"><select class="form-control m-b" name="id_client">
									@foreach($clients as $client)
                                        <option value="{{$client->id_client}}">{{$client->nom}} {{$client->prenom}}</option>
									@endforeach()
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Mode de paiement :</label>

                                <div class="col-sm-10"><select class="form-control m-b" name="id_mode_paiement">
									@foreach($mode_paiements as $mode_paiement)
                                        <option value="{{$mode_paiement->id_mode_paiement}}">{{$mode_paiement->libele_mp}}</option>
									@endforeach()
                                    </select>
                                </div>
                            </div>
							
							<div class="form-group"><label class="col-sm-2 control-label">Type d'abonnement :</label>

                                <div class="col-sm-10"><select class="form-control m-b" name="id_abonnement">
									@foreach($abonnements as $abonnement)
                                        <option value="{{$abonnement->id_abonnement}}">{{$abonnement->duree}} / {{$abonnement->descreptif}}</option>
									@endforeach()
                                    </select>
                                </div>
                            </div>

							<div class="form-group"><label class="col-sm-2 control-label">Date reception :</label>
                                <div class="col-sm-10"><input type="date" name="date_reception" class="form-control"
                                                              required="required"></div>
                            </div>
							
							<div class="form-group"><label class="col-sm-2 control-label">Date encaissement :</label>
                                <div class="col-sm-10"><input type="date" name="date_encaissement" class="form-control"
                                                              required="required"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Descreptif :</label>
                                <div class="col-sm-10"><textarea name="desc" class="form-control"
                                                                 required="required"></textarea></div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Ajouter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection