<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>N9IYA</title>

    <!-- Favicon -->
    <link rel="icon" href="{{URL::asset('public/user/img/core-img/favicon.jpg')}}">

    <!-- Core Stylesheet -->
	<link href="{{URL::asset('public/user/style.css')}}" rel="stylesheet">

    <!-- Responsive CSS -->
	<link href="{{URL::asset('public/user/css/responsive.css')}}" rel="stylesheet">

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-10">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="#">n9iya</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item active"><a class="nav-link" href="#home">Acceuil</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">Services</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#pricing">Abonnements</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#testimonials">Plus</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!--
                <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="#">Sign Up Free</a>
                    </div>
                </div>
                -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="wellcome_area clearfix" id="home">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-md">
                    <div class="wellcome-heading">
                        <h2>ET SI DEMAIN,
						<br>VOUS NE FAISIEZ PLUS LE MÉNAGE ?</h2><br>
                        <h3><img src="{{URL::asset('public/user/img/core-img/logo_blanc.png')}}" alt="yy"></h3>
                        <h4>Retrouvez votre maison propre 
						<br>et rangée grâce à nos femmes de ménage
						<br>rigoureusement sélectionnées</h4>
                    </div>
                    <div class="get-start-area">
                    </div>
                </div>
            </div>
        </div>
        <!-- Welcome thumb 
        <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
            <img src="public/user/img/bg-img/welcome-img.png" alt="">
        </div>-->
    </section>
    <!-- ***** Wellcome Area End ***** -->

    <!-- ***** Special Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-center">
                        <h2>Pourquoi nous ?</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="single-icon">
                            <i class="ti-mobile" aria-hidden="true"></i>
                        </div>
                        <h4>Demande rapide</h4>
                        <p>On vous garantis un service rapide, en traitant vos demandes sur place<br><br><br></p>
                    </div>
                </div>
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="single-icon">
                            <i class="ti-ruler-pencil" aria-hidden="true"></i>
                        </div>
                        <h4>Disponibilité</h4>
                        <p>On vous offre un service avec une disponibilité permanente, durant toute la journée et surtout les weekends<br><br></p>
                    </div>
                </div>
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                        <div class="single-icon">
                            <i class="ti-settings" aria-hidden="true"></i>
                        </div>
                        <h4>Sécurité</h4>
                        <p>Nos agents sont digne de confiance, elles sont choisis minitieusement à travers un processus de recrutement bien approprié</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Special Description Area -->
        <div class="special_description_area mt-150">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="special_description_img">
                            <img src="{{URL::asset('public/user/img/bg-img/special.png')}}" alt="special">
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 ml-xl-auto">
                        <div class="special_description_content">
                            <h2>N9IYA</h2>
                            <p>N9IYA est un service qui vous permet de réserver un créneau horaire ou une femme de ménage vous sera envoyé à travers des abonnments avec des prix exceptionnels, tout cela via un processus simple ou quelque clics vous en sépare.</p>
                            <div class="app-download-area">
                                <div class="app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                    <!-- Google Store Btn -->
                                    <a href="#">
                                        <i class="fa fa-android"></i>
                                        <p class="mb-0"><span>available on</span> Google Store</p>
                                    </a>
                                </div>
                                <div class="app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                                    <!-- Apple Store Btn -->
                                    <a href="#">
                                        <i class="fa fa-apple"></i>
                                        <p class="mb-0"><span>available on</span> Apple Store</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Special Area End ***** -->

    <!-- ***** Awesome Features Start ***** -->
    <section class="awesome-feature-area bg-white section_padding_0_50 clearfix" id="features">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Heading Text -->
                    <div class="section-heading text-center">
                        <h2>Awesome Features</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-user" aria-hidden="true"></i>
                        <h5>Awesome Experience</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-pulse" aria-hidden="true"></i>
                        <h5>Fast and Simple</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-dashboard" aria-hidden="true"></i>
                        <h5>Clean Code Tezst</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-palette" aria-hidden="true"></i>
                        <h5>Perfect Design</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-crown" aria-hidden="true"></i>
                        <h5>Best Industry Leader</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-headphone" aria-hidden="true"></i>
                        <h5>24/7 Online Support</h5>
                        <p>Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- ***** Awesome Features End ***** -->


    
<!--
    // ***** App Screenshots Area Start *****
    <section class="app-screenshots-area bg-white section_padding_0_100 clearfix" id="screenshot">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    // Heading Text
                    <div class="section-heading">
                        <h2>App Screenshots</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    // App Screenshots Slides
                    <div class="app_screenshots_slides owl-carousel">
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-1.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-2.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-3.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-4.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-5.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="public/user/img/scr-img/app-3.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    // ***** App Screenshots Area End *****======
-->
    <!-- ***** Pricing Plane Area Start *****==== -->
    <section class="pricing-plane-area section_padding_100_70 clearfix" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Heading Text  -->
                    <div class="section-heading text-center">
                        <h2>Abonnement Plan</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>

                                                
                                           
            <div class="row no-gutters">
				@foreach($abonnements as $abonnement) 
					@if($abonnement->titre != 'Liberté Plan')
						<div class="col-12 col-md-6 col-lg-3">
							<!-- Package Price  -->
							<div class="single-price-plan text-center">
								<!-- Package Text  -->
								<div class="package-plan">
									<h5>{{$abonnement->titre}}</h5>
									<div class="ca-price d-flex justify-content-center">
										<h4>{{$abonnement->solde }}</h4>
										<span>DH/mois</span>
									</div>
								</div>
								<div class="package-description">
									<p>Faire les <b>poussières</b></p>
									<p>Aspirer et nettoyer les <b>sols</b></p>
									<p>Nettoyer la <b>cuisine</b> et la <b>salle de bain</b> </p>
									<p>Étendre vos <b>lessives</b></p>
									<p>Repasser vos <b>chemises</b></p>
									<p>{{$abonnement->descreptif}}</b></p>
								</div>
								<!-- Plan Button  -->
								<div class="plan-button">
									<a href="#">Select Plan</a>
								</div>
							</div>
						</div>
					@else
						<div class="col-12 col-md-6 col-lg-3">
							<!-- Package Price  -->
							<div class="single-price-plan text-center">
								<!-- Package Text  -->
								<div class="package-plan">
									<h5>{{$abonnement->titre}}</h5>
									<div class="ca-price d-flex justify-content-center">
										<h4>{{$abonnement->solde }}</h4>
										<span>DH</span>
									</div>
								</div>
								<div class="package-description">
									<p>Faire les <b>poussières</b></p>
									<p>Aspirer et nettoyer les <b>sols</b></p>
									<p>Nettoyer la <b>cuisine</b> et la <b>salle de bain</b> </p>
									<p>Étendre vos <b>lessives</b></p>
									<p>Repasser vos <b>chemises</b></p>
									<p>{{$abonnement->descreptif}}</b></p>
								</div>
								<!-- Plan Button  -->
								<div class="plan-button">
									<a href="#">Select Plan</a>
								</div>
							</div>
						</div>
					@endif
				@endforeach()
            </div>
        </div>
    </section>
    <!-- ***** Pricing Plane Area End ***** -->
	

    <!-- ***** Client Feedback Area Start ***** -->
    <section class="clients-feedback-area bg-white section_padding_100 clearfix" id="testimonials">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <div class="slider slider-for">
                        <!-- Client Feedback Text  -->
                        <div class="client-feedback-text text-center">
                            <div class="client">
                                <i>MAIS AUSSI :</i>
                            </div>
                            <div class="client-description text-center">
                                <p>Récupérer vos <b>courses au drive</b>, <b>ranger</b> votre maison, nettoyer les <b>appareils électroménagers</b>, laver les <b>vitres...</b></p>
                            </div>
                            <div class="star-icon text-center">
                                <i class="ion-ios-star"></i>
                                <i class="ion-ios-star"></i>
                                <i class="ion-ios-star"></i>
                                <i class="ion-ios-star"></i>
                                <i class="ion-ios-star"></i>
                            </div>
							<!--
                            <div class="client-name text-center">
                                <h5>Aigars Silkalns</h5>
                                <p>Ceo Colorlib</p>
                            </div>
							-->
                        </div>
                         
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- ***** Client Feedback Area End ***** -->
<!-- ***** Cool Facts Area Start ***** 
    <section class="cool_facts_area clearfix">
        <div class="container">
            <div class="row">
                <!-- Single Cool Fact
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="counter-area">
                            <h3><span class="counter">90</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-arrow-down-a"></i>
                            <p>APP <br> DOWNLOADS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact 
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="counter-area">
                            <h3><span class="counter">20</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-happy-outline"></i>
                            <p>Clients <br> contents</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact 
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.6s">
                        <div class="counter-area">
                            <h3><span class="counter">40</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-person"></i>
                            <p>ACTIVE <br>ACCOUNTS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact 
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.8s">
                        <div class="counter-area">
                            <h3><span class="counter">10</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-ios-star-outline"></i>
                            <p>TOTAL <br>APP RATES</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Cool Facts Area End ***** -->
    <!-- ***** CTA Area Start ***** 
    <section class="our-monthly-membership section_padding_50 clearfix">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="membership-description">
                        <h2>Join our Monthly Membership</h2>
                        <p>Find the perfect plan for you — 100% satisfaction guaranteed.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="get-started-button wow bounceInDown" data-wow-delay="0.5s">
                        <a href="#">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** CTA Area End ***** -->

    <!-- ***** Our Team Area Start ***** 
    <section class="our-Team-area bg-white section_padding_100_50 clearfix" id="team">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    
                    <div class="section-heading">
                        <h2>Our Team</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-team-member">
                        <div class="member-image">
                            <img src="public/user/img/team-img/team-1.jpg" alt="">
                            <div class="team-hover-effects">
                                <div class="team-social-icon">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="member-text">
                            <h4>Jackson Nash</h4>
                            <p>Tax Advice</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-team-member">
                        <div class="member-image">
                            <img src="public/user/img/team-img/team-2.jpg" alt="">
                            <div class="team-hover-effects">
                                <div class="team-social-icon">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="member-text">
                            <h4>Alex Manning</h4>
                            <p>CEO-Founder</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-team-member">
                        <div class="member-image">
                            <img src="public/user/img/team-img/team-3.jpg" alt="">
                            <div class="team-hover-effects">
                                <div class="team-social-icon">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="member-text">
                            <h4>Ollie Schneider</h4>
                            <p>Business Planner</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-team-member">
                        <div class="member-image">
                            <img src="public/user/img/team-img/team-4.jpg" alt="">
                            <div class="team-hover-effects">
                                <div class="team-social-icon">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="member-text">
                            <h4>Roger West</h4>
                            <p>Financer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Our Team Area End ***** -->

    <!-- ***** Contact Us Area Start ***** -->
    <section class="footer-contact-area section_padding_100 clearfix" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- Heading Text  -->
                    <div class="section-heading">

                        <h2>Contactez-nous!</h2>
                        <div class="line-shape"></div>
                    </div>
                    <div class="footer-text">
                        <p>vous pouvez remplir le formulaire pour demander une femme femme de ménage ou également nous contacter directement par téléphone ou email.</p>
                    </div>
                    <div class="address-text">
                        <p><span>Address:</span> La belle vue Sala Jadida, Maroc.</p>
                    </div>
                    <div class="phone-text">
                        <p><span>Tél:</span> +212 655 64 33 28</p>

                    </div>
                    <div class="email-text">
                        <p><span>Email:</span> info@n9iya.com</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Form Start-->
                    <div class="contact_from">
                        <form action="{{route('commande')}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="vu" value="0"/>
                            <!-- Message Input Area Start -->
                            <div class="contact_input_area">
                                <div class="row">
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom compet" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="tel" id="tel" placeholder="Numéro téléphone" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" name="id_abonnement" required>
                                                @foreach($abonnements as $abonnement)
                                                    <option value="{{$abonnement->id_abonnement}}">{{$abonnement->titre}} -- {{$abonnement->solde}} DH</option>
                                                @endforeach()
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="mail"  placeholder="E-mail" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control"  cols="30" rows="4" placeholder="Votre message *" required></textarea>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <button type="submit" class="btn submit-btn">Commander Maintenant</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Message Input Area End -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon text-center section_padding_70 clearfix">
        <!-- footer logo -->
        <div class="footer-text">
            <h2>N9IYA</h2>
        </div>
        <!-- social icon-->
        <div class="footer-social-icon">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="active fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        </div>
        <div class="footer-menu">
            <nav>
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div>
        <!-- Foooter Text-->
        <div class="copyright-text">
            <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
            <p>Copyright ©2018 N9IYA</p>
        </div>
    </footer>
    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
	<script src="{{URL::asset('public/user/js/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
	<script src="{{URL::asset('public/user/js/popper.min.js')}}"></script>
    <!-- Bootstrap-4 Beta JS -->
	<script src="{{URL::asset('public/user/js/bootstrap.min.js')}}"></script>
    <!-- All Plugins JS -->
	<script src="{{URL::asset('public/user/js/plugins.js')}}"></script>
    <!-- Slick Slider Js-->
	<script src="{{URL::asset('public/user/js/slick.min.js')}}"></script>
    <!-- Footer Reveal JS -->
	<script src="{{URL::asset('public/user/js/footer-reveal.min.js')}}"></script>
    <!-- Active JS -->
	<script src="{{URL::asset('public/user/js/active.js')}}"></script>
</body>

</html>
