-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 22 oct. 2018 à 15:54
-- Version du serveur :  5.7.21
-- Version de PHP :  7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `x_clean`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnements`
--

DROP TABLE IF EXISTS `abonnements`;
CREATE TABLE IF NOT EXISTS `abonnements` (
  `id_abonnement` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duree` int(11) NOT NULL,
  `descreptif` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `solde` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_abonnement`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `abonnements`
--

INSERT INTO `abonnements` (`id_abonnement`, `titre`, `duree`, `descreptif`, `solde`, `created_at`, `updated_at`) VALUES
(5, 'Simple Plan', 90, '1 fois(1h30mn)/semaine .', 299.00, '2018-10-22 13:12:30', '2018-10-22 13:56:27'),
(6, 'Basic Plan', 75, '2 fois (2h30mn)/semaine', 499.00, '2018-10-22 13:59:16', '2018-10-22 13:59:16'),
(7, 'Recommande Plan', 225, '3 fois (3h45h)/semaine', 699.00, '2018-10-22 14:03:14', '2018-10-22 14:03:14'),
(8, 'Top Plan', 250, '5 fois (4h10mn)/semaine', 719.00, '2018-10-22 14:05:08', '2018-10-22 14:05:08');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id_client` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` enum('madame','Monsieur','entreprise') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_client`),
  UNIQUE KEY `clients_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id_client`, `nom`, `prenom`, `sexe`, `email`, `telephone`, `adresse`, `cin`, `created_at`, `updated_at`) VALUES
(5, 'Malika', 'Khadija', 'madame', 'simo.robin.995@gmail.com', '0655643328', 'hay hay', 'P2761187', '2018-07-02 21:10:55', '2018-07-02 21:10:55');

-- --------------------------------------------------------

--
-- Structure de la table `depenses`
--

DROP TABLE IF EXISTS `depenses`;
CREATE TABLE IF NOT EXISTS `depenses` (
  `id_depense` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_mode_paiement` int(10) UNSIGNED NOT NULL,
  `id_type_depense` int(10) UNSIGNED NOT NULL,
  `solde` double(8,2) NOT NULL,
  `date_reception` datetime NOT NULL,
  `date_encaissement` datetime NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_depense`),
  KEY `depenses_id_employee_foreign` (`id_employee`),
  KEY `depenses_id_mode_paiement_foreign` (`id_mode_paiement`),
  KEY `depenses_id_type_depense_foreign` (`id_type_depense`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `depenses`
--

INSERT INTO `depenses` (`id_depense`, `id_employee`, `id_mode_paiement`, `id_type_depense`, `solde`, `date_reception`, `date_encaissement`, `desc`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 3, 875454.00, '5215-02-12 00:00:00', '2120-12-20 00:00:00', 'sdvdfvx sfvdfvfd', '2018-08-07 09:19:27', '2018-08-07 09:19:27'),
(3, 1, 1, 2, 2000.00, '2500-02-12 00:00:00', '2100-12-20 00:00:00', 'walo walo', '2018-10-01 08:06:27', '2018-10-01 08:06:27');

-- --------------------------------------------------------

--
-- Structure de la table `disponibilites`
--

DROP TABLE IF EXISTS `disponibilites`;
CREATE TABLE IF NOT EXISTS `disponibilites` (
  `id_disponibilite` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_client` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `jour` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_disponibilite`),
  KEY `disponibilites_id_client_foreign` (`id_client`),
  KEY `disponibilites_id_employee_foreign` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id_employee` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` enum('madame','Monsieur') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_employee`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `employees`
--

INSERT INTO `employees` (`id_employee`, `nom`, `prenom`, `sexe`, `email`, `telephone`, `adresse`, `cin`, `created_at`, `updated_at`) VALUES
(1, 'Halima', 'koko', 'madame', 'halima@hm.com', '06772762', 'hay ahaya hay a', 'X285252', '2018-08-07 09:15:19', '2018-08-07 09:15:19');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_20_151703_create_clients_table', 1),
(4, '2018_05_20_152854_create_mode_paiements_table', 2),
(5, '2018_05_20_153137_create_abonnements_table', 3),
(6, '2018_05_20_153906_create_ressources_table', 4),
(7, '2018_05_20_154241_create_employees_table', 5),
(8, '2018_05_20_154401_create_type_depenses_table', 6),
(9, '2018_05_20_154542_create_depenses_table', 7),
(10, '2018_06_29_192522_create_disponibilites_table', 8),
(11, '2018_07_02_224111_add_created_at_and_update_at_to_table_employee', 9);

-- --------------------------------------------------------

--
-- Structure de la table `mode_paiements`
--

DROP TABLE IF EXISTS `mode_paiements`;
CREATE TABLE IF NOT EXISTS `mode_paiements` (
  `id_mode_paiement` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libele_mp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mode_paiement`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mode_paiements`
--

INSERT INTO `mode_paiements` (`id_mode_paiement`, `libele_mp`, `created_at`, `updated_at`) VALUES
(1, 'espece', NULL, '2018-07-03 20:48:45'),
(2, 'cheque', '2018-07-03 20:57:14', '2018-07-03 20:57:21');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ressources`
--

DROP TABLE IF EXISTS `ressources`;
CREATE TABLE IF NOT EXISTS `ressources` (
  `id_ressource` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_client` int(10) UNSIGNED NOT NULL,
  `id_mode_paiement` int(10) UNSIGNED NOT NULL,
  `id_abonnement` int(10) UNSIGNED NOT NULL,
  `date_reception` datetime NOT NULL,
  `date_encaissement` datetime NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_ressource`),
  KEY `ressources_id_client_foreign` (`id_client`),
  KEY `ressources_id_mode_paiement_foreign` (`id_mode_paiement`),
  KEY `ressources_id_abonnement_foreign` (`id_abonnement`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `type_depenses`
--

DROP TABLE IF EXISTS `type_depenses`;
CREATE TABLE IF NOT EXISTS `type_depenses` (
  `id_type_depense` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libele_td` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_type_depense`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `type_depenses`
--

INSERT INTO `type_depenses` (`id_type_depense`, `libele_td`, `created_at`, `updated_at`) VALUES
(2, 'sasa shg', '2018-08-05 15:44:04', '2018-08-05 15:56:21');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mohamed', 'mohamed@gmail.com', '$2y$10$wkZ9b./VZpZq6oGcuxbL7./bBebeOczkA1bQ7jsJjL0j.Yj4MfiyG', 'MnTQ2PMUiMxZTPtmQwur9YIJJrSWyxWfdh1zUCGS0uW6WCJFyhJBWY46jTlk', '2018-05-20 16:03:00', '2018-05-20 16:03:00');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `disponibilites`
--
ALTER TABLE `disponibilites`
  ADD CONSTRAINT `disponibilites_id_client_foreign` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`),
  ADD CONSTRAINT `disponibilites_id_employee_foreign` FOREIGN KEY (`id_employee`) REFERENCES `employees` (`id_employee`);

--
-- Contraintes pour la table `ressources`
--
ALTER TABLE `ressources`
  ADD CONSTRAINT `ressources_id_abonnement_foreign` FOREIGN KEY (`id_abonnement`) REFERENCES `abonnements` (`id_abonnement`),
  ADD CONSTRAINT `ressources_id_client_foreign` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`),
  ADD CONSTRAINT `ressources_id_mode_paiement_foreign` FOREIGN KEY (`id_mode_paiement`) REFERENCES `mode_paiements` (`id_mode_paiement`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
